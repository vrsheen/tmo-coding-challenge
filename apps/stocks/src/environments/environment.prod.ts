import { StocksAppConfig } from '@coding-challenge/stocks/data-access-app-config';

export const environment: StocksAppConfig = {
  production: true,
  apiKey: 'pk_ba0a65512ed84f20b0b5040bba2987c6',
  apiURL: 'https://sandbox.iexapis.com'
};
