import { CommonModule, DatePipe } from '@angular/common';
import { NgModule } from '@angular/core';
import { GoogleChartsModule } from 'angular-google-charts';
import { ChartComponent } from './chart/chart.component';
import {
  MatFormFieldModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatInputModule,
  MAT_DATE_FORMATS
} from '@angular/material';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  imports: [CommonModule, GoogleChartsModule.forRoot(), MatInputModule,
    MatDatepickerModule, MatFormFieldModule, MatNativeDateModule, ReactiveFormsModule],
  declarations: [ChartComponent],
  exports: [ChartComponent],
  providers: [DatePipe,]
})
export class SharedUiChartModule { }
