import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit
} from '@angular/core';
import { Observable } from 'rxjs';
import { FormBuilder, FormGroup, Validators, ValidatorFn } from '@angular/forms';
import { DatePipe } from '@angular/common';

@Component({
  selector: 'coding-challenge-chart',
  templateUrl: './chart.component.html',
  styleUrls: ['./chart.component.css']
})
export class ChartComponent implements OnInit {
  stockChartForm: FormGroup;
  minFrmDt: Date; maxToDt: Date;
  @Input() data$: Observable<any>;
  chartData: Array<any>;
  filterData;
  chart: {
    type: string;
    columnNames: string[];
    options: any;
  };

  EffDtValidator: ValidatorFn;
  ExpDtValidator: ValidatorFn;

  constructor(private cd: ChangeDetectorRef, private fb: FormBuilder, private datePipe: DatePipe) {
    this.EffDtValidator = () => {
      if (this.stockChartForm) {
        const frmDt: Date = this.stockChartForm.get("frmDt").value;
        const toDt: Date = this.stockChartForm.get("toDt").value;
        if (frmDt && toDt) {
          if (frmDt.getTime() >= toDt.getTime()) {
            return { frmGtTnToDt: true };
          }
        }
      }
    };

    this.ExpDtValidator = () => {
      if (this.stockChartForm) {
        const frmDt: Date = this.stockChartForm.get("frmDt").value;
        const toDt: Date = this.stockChartForm.get("toDt").value;
        if (frmDt && toDt) {
          if (toDt.getTime() <= frmDt.getTime()) {
            return { toGtTnFrmDt: true };
          }
        }
      }
    };


    this.stockChartForm = fb.group({
      frmDt: [null, [this.EffDtValidator]],
      toDt: [null, [this.ExpDtValidator]]
    });


  }

  ngOnInit() {
    this.data$.subscribe(newData => {
      this.chartData = newData;

      if (this.chartData) {
        this.minFrmDt = new Date(this.chartData[0][0]);
        this.maxToDt = new Date(this.chartData[this.chartData.length - 1][0]);
        this.minFrmDt.setMinutes(this.minFrmDt.getMinutes() + this.minFrmDt.getTimezoneOffset());
        this.maxToDt.setMinutes(this.maxToDt.getMinutes() + this.maxToDt.getTimezoneOffset());
        this.stockChartForm.get('frmDt').setValue(this.minFrmDt);
        this.stockChartForm.get('toDt').setValue(this.maxToDt);
      }

      this.dtVlChangFlt();
    });

    this.chart = {
      type: 'LineChart',
      columnNames: ['Period', 'Closing Price'],
      options: { title: `Stock Price Closing Trend`, width: '600', height: '400' }
    };

  }

  dtVlChangFlt() {
    this.filterData = JSON.parse(JSON.stringify(this.chartData));
    this.stockChartForm.get('frmDt').valueChanges.subscribe(val => {
      this.filterChrData();
    });
    this.stockChartForm.get('toDt').valueChanges.subscribe(val => {
      this.filterChrData();
    });
  }

  filterChrData() {
    const frmDt: Date = this.stockChartForm.get("frmDt").valid ? this.stockChartForm.get("frmDt").value : null;
    const toDt: Date = this.stockChartForm.get("toDt").valid ? this.stockChartForm.get("toDt").value : null;
    if (frmDt && toDt && this.chartData) {
      this.filterData = this.chartData.filter(data => new Date(data[0]).getTime() >= frmDt.getTime()
        && new Date(data[0]).getTime() <= toDt.getTime());
    } else {
      this.filterData = null;
    }
  }
}
